#!/usr/bin/env python
import os, sys, subprocess, datetime

import base64, json
from urllib.request import Request, urlopen
from urllib.parse import urlencode

client_id = "48eeb8c2c011861"

def imgur_upload(title, image_data):
  headers = {'Authorization': 'Client-ID ' + client_id}
  req = Request("https://api.imgur.com/3/upload.json", data=image_data, headers=headers)
  response = urlopen(req).read()
  parse = json.loads(response)
  return parse['data']['link']

screenshot_dir = "~/Images/Screenshot"
screenshot_tool = "maim"
screenshot_args = ['-o']
screenshot_format = "[%Y%m%d]_%H%M%S_screenshot.png"

if not os.path.exists(os.path.expanduser(screenshot_dir)):
  screenshot_dir = "~"
if len(sys.argv) > 1:
  screenshot_args += sys.argv[1:]
print("Taking screenshot")
program = subprocess.run([screenshot_tool]+screenshot_args, stdout=subprocess.PIPE)
image = program.stdout
screenshot_name = datetime.datetime.now().strftime(screenshot_format)
print("Asking for instruction")
kdialog = subprocess.run(['kdialog', '--title', 'Maim Chain', '--checklist', 'What would you like to do?', '1', 'Save to file', 'off', '2', 'Upload to Imgur', 'on'], stdout=subprocess.PIPE)
choice = kdialog.stdout.decode().replace('"', '').split(' ')
if '1' in choice:
  print("Writing image to file")
  with open(os.path.expanduser('{}/{}'.format(screenshot_dir,screenshot_name)), 'wb') as f:
    f.write(image)
if '2' in choice:
  print("Uploading to imgur script")
  imgur_link = imgur_upload(screenshot_name, image)
  #imgur = subprocess.run(['imgur.sh', '-'], input=image, stdout=subprocess.PIPE)
  #imgur_link = imgur.stdout.decode().rstrip()
  print("Adding to klipper: {}".format(imgur_link))
  cp2klipper = subprocess.run(['qdbus', 'org.kde.klipper', '/klipper', 'setClipboardContents', imgur_link.encode('utf-8')])
print("Completed screenshot")
