#!/bin/bash
case "$1" in
    'start')
        ;&
    'crd')
        crd --start &
        if [[ $1 -eq 'crd' ]]; then exit; fi
        ;&
    'ngrok')
        #tmux -f /home/jplayer/.config/tmux/tmux.conf new-session -Ad -t Tilda &
        tmux -f /home/jplayer/.config/tmux/tmux.conf new-window -dP -t Tilda -n ngrok ngrok tcp 22 --log false > /dev/null &
        if [[ $1 -eq 'ngrok' ]]; then exit; fi
        ;;
    'stop')
        crd --stop &
        tmux kill-window -t Tilda:ngrok &
        ;;
    *)
        echo "Unknown argument"
esac
