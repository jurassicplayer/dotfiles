#!/bin/bash
LSEP=
LSEPE=
RSEP=
RSEPE=

CLOCK=⌚
CALENDAR=☼
MUSIC=♫

WIDTH=${1}
BG_COLOR=${2}

SMALL=80
MEDIUM=140

#mpd_fg_color='colour235'
#mpd_bg_color='colour252'
#date_fg_color='colour232'
#date_bg_color='colour240'
#time_fg_color='colour232'
#time_bg_color='colour245'

if [ "$WIDTH" -gt "$SMALL" ]; then
  if mpc | grep playing > /dev/null ; then
    FG_COLOR=white
  else
    FG_COLOR=black
  fi
  MPD="$RSEP#[fg=green, bg=red]$RSEP#[fg=blue, bg=green]$RSEP#[fg=yellow, bg=blue]$RSEP#[fg=$FG_COLOR, bg=yellow] $MUSIC #[fg=black]$(mpc current)"
  #MPD="#[fg=${mpd_bg_color},bg=default,nobold,noitalics,nounderscore]$RSEP#[fg=${mpd_fg_color},bg=${mpd_bg_color},bold,noitalics,nounderscore] $MUSIC $(mpc current) "
  #date_colour=$mpd_bg_color
fi

#DATE="#[fg=${date_bg_color},bg=${date_colour:-default},nobold,noitalics,nounderscore]$RSEP#[fg=${date_fg_color},bg=${date_bg_color},nobold,noitalics,nounderscore]"
# TIME="#[fg=${time_bg_color},bg=${date_bg_color},nobold,noitalics,nounderscore]$RSEPE#[fg=${time_fg_color},bg=${time_bg_color},nobold,noitalics,nounderscore] $(date +'%I:%H:%M')"

echo "$MPD" | sed 's/ *$/ /g'
