from ranger.gui.colorscheme import ColorScheme
from ranger.gui.color import *

class ColorScheme(ColorScheme):

    def use(self, context):
        fg, bg, attr = default_colors

        if context.reset:
            return default_colors

        elif context.in_browser:
            if context.selected:
                attr = reverse
            else:
                attr = normal
            if context.empty or context.error:
                fg = black
                bg = 9
            if context.border:
                fg = white
            if context.document:
                fg = white
            if context.image:
                fg = magenta
            if context.video:
                fg = blue
            if context.audio:
                fg = green
            if context.container:
                # Archive files
                fg = cyan
            if context.directory:
                attr |= normal
                fg = yellow
            elif context.executable and not \
                    any((context.media, context.container,
                       context.fifo, context.socket)):
                #attr |= bold
                fg = red
            if context.socket:
                fg = blue
                attr |= bold
            if context.fifo or context.device:
                fg = blue
                if context.device:
                    attr |= bold
            if context.link:
                attr |= bold
                fg = context.good and yellow or red
            if context.bad:
                fg = yellow
                bg = 9
            if context.tag_marker and not context.selected:
                #attr |= bold
                fg = red
            if not context.selected and (context.cut or context.copied):
                attr = reverse
            if context.main_column:
                #if context.selected:
                #    attr |= bold
                if context.marked:
                #    attr |= bold
                    attr |= reverse
            if context.badinfo:
                if attr & reverse:
                    bg = red
                else:
                    fg = cyan

        elif context.in_titlebar:
            #attr |= bold
            if context.hostname:
                fg = context.bad and red or magenta
            elif context.directory:
                fg = yellow
            elif context.tab:
                if context.good:
                    bg = green

        elif context.in_statusbar:
            if context.permissions:
                if context.good:
                    fg = green
                    bg = black
                elif context.bad:
                    fg = red
            if context.marked:
                attr |= bold | reverse
                fg = red
            if context.message:
                if context.bad:
                    attr |= bold
                    fg = red
            if context.loaded:
                bg = red


        if context.text:
            if context.highlight:
                attr |=  reverse

        if context.in_taskview:
            if context.title:
                fg = blue

            if context.selected:
                attr |= reverse

            if context.loaded:
                if context.selected:
                    fg = red
                else:
                    bg = red

        return fg, bg, attr
