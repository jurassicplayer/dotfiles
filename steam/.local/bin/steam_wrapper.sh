#!/bin/bash
[[ ! -d "$XDG_DATA_HOME"/Steam ]] && mkdir -p "$XDG_DATA_HOME"/Steam
HOME="$XDG_DATA_HOME"/Steam steam "$@"