#!/bin/bash
function install()
{
  stow_ready=$(if pacman -Qi $1 > /dev/null ; then echo 1 ; fi)
  if [[ -z $stow_ready ]]; then
    echo "$1 does not seem to be installed."
    echo "Installing $1..."
    if [[ -n $(if command -v yay > /dev/null ; then echo 1 ; fi) ]]; then
      echo "using yay."
      yay -S --noconfirm $1
    elif [[ -n $(if command -v aurman > /dev/null ; then echo 1 ; fi) ]]; then
      echo "using aurman."
      sudo aurman -S --noconfirm $1
    else
      echo "using pacman."
      sudo pacman -S --noconfirm $1
    fi
  else
    echo "$1 is already installed."
  fi
}

function mkcfgdir() {
  if [[ -z $XDG_CONFIG_HOME ]]; then XDG_CONFIG_HOME="$HOME/.config"; fi
  if [[ ! -d "$XDG_CONFIG_HOME/$1" ]]; then
    mkdir -p $XDG_CONFIG_HOME/$1
  fi
}

function symlink()
{
  if [[ -d "$script_path/$1" ]]; then
    echo ""
    echo "--[ $1 ]--"
    echo "$1 dotfiles found."
    case "$1" in
    # Do all program-specific pre-symlink handling of files here
    'aurman')
      if [[ -z $(if command -v $1 > /dev/null ; then echo 1 ; fi) ]]; then
        install git
        PWDo=$PWD
        git clone https://aur.archlinux.org/aurman.git ~/aurman_build
        cd ~/aurman_build
        makepkg -si
        cd $PWDo
      else
        echo "$1 is already installed."
      fi
      ;;
    'yay')
      if [[ -z $(if command -v $1 > /dev/null ; then echo 1 ; fi) ]]; then
        install git
        PWDo=$PWD
        git clone https://aur.archlinux.org/yay.git ~/yay_build
        cd ~/yay_build
        makepkg -si
        cd $PWDo
        rm -rf ~/yay_build
      else
        echo "$1 is already installed."
      fi
      ;;
    'bin')
      ;;
    'firefox')
      install $1
      FFPROFILES=($(cat ~/.mozilla/firefox/profiles.ini | grep Path))
      if [[ -n $FFPROFILES ]]; then
        echo "[Firefox Profiles]"
        for (( i = 0; i < ${#FFPROFILES}; i++));
        do
          if [[ -n ${FFPROFILES[$i]} ]]; then
            iterator=$(( $i+1 ))
            echo "$iterator) ${FFPROFILES[$i]:5}"
          fi
        done
        read -p "Select a profile to install FirefoxCSS (default: all): " USERPROFILE
      else
        echo "No firefox profiles found."
        echo "Please initialize a firefox profile and try again."
        return 1
      fi
      ;;
    'ncmpcpp')
      install ncmpcpp
      mkcfgdir $1
      touch $XDG_CONFIG_HOME/ncmpcpp/error.log
      ;;
    'mpd')
      install mpd
      mkcfgdir mpd/playlists
      ;;
   'neovim')
      install neovim
      mkcfgdir nvim/autoload
      ;;
    'rxvt-unicode')
      ;&
    'urxvt')
      install rxvt-unicode
      echo 'urxvt dotfiles depend on Xresources dotfiles, symlinking .dotfiles.'
      echo "The app Xresource must be manually added via #include in the Xresources."
      ;;
    'startx')
      ;&
    'xinit')
      ;&
    'xorg-xinit')
      if [[ -z $(if pacman -Qi xorg-xinit > /dev/null ; then echo 1 ; fi) ]]; then
        echo "$1 should only start an Xsession when no display manager is available."
        read -p "Do you wish to continue ([y]es/[N]o)? " install_xinit
        case "$install_xinit" in
          [yY]*)
            install xorg-xinit
            ;;
          *)
            return 0
            ;;
        esac
      else
        echo "xorg-xinit is already installed."
      fi
      ;;
    'Xresources')
      ;;
    'zsh')
      install $1
      if [[ ! $SHELL == '/bin/zsh' ]]; then
        echo "Changing default shell to zsh."
        chsh -s /bin/zsh $(whoami)
      fi
      if [[ -z $(if command -v antibody > /dev/null ; then echo 1 ; fi) ]]; then
        echo "Installing antibody package manager."
        curl -sL git.io/antibody | sh -s
      fi
      if [[ -z $XDG_CONFIG_HOME ]]; then XDG_CONFIG_HOME="$HOME/.config"; fi
      if [[ ! -d "$XDG_CONFIG_HOME/zsh" ]]; then
        mkdir -p $XDG_CONFIG_HOME/zsh
      fi
      # Touching some files in $XDG_CONFIG_HOME/zsh ensures that stow will symlink by file instead of folder
      touch $XDG_CONFIG_HOME/zsh/histfile
      touch $XDG_CONFIG_HOME/zsh/privatekey
      ;;
    *)
      install $1
      ;;
    esac
    
    echo "Symlinking stowed $1 dotfiles."
    case $1 in 
      'firefox')
        if [[ -n $USERPROFILE ]]; then
          USERPROFILE=$(( $USERPROFILE-1 ))
          FFPROFILES=(${FFPROFILES[$USERPROFILE]})
        fi
        for (( i = 0; i < ${#FFPROFILES}; i++));
        do
          if [[ -n ${FFPROFILES[$i]} ]]; then
            echo "Symlinking stowed chrome files to ${FFPROFILES[$i]:5}"
            stow -t "$HOME/.mozilla/firefox/${FFPROFILES[$i]:5}" -d "$script_path" firefox
          fi
        done
        return 0
        ;;
      'ncmpcpp')
        symlink mpd
        stow -t $HOME -R -d "$script_path" $1
        ;;
      *)
        # Symlink to $HOME
        stow -t $HOME -R -d "$script_path" $1
        ;;
    esac
  else
    echo "No corresponding $1 dotfiles found."
  fi
}


check_for_stow() {
  if [[ -z $(if command -v stow > /dev/null ; then echo 1 ; fi) ]]; then
    echo "My sensors indicate that you don't have GNU stow, which this script depends on to symlink."
    read -p "Would you like to install stow ([y]es/[n]o)? " install_stow
    case "$install_stow" in
      [yY]*)
        install stow
        ;;
      *)
        exit
        ;;
    esac
  fi
}

script_path=`dirname "$0"`
case "$1" in 
  '-a')
    check_for_stow
    echo "Installing all symlinks."
    for DIR in `find $script_path -mindepth 1 -maxdepth 1 -type d`
    do
      if [[ ! ${DIR:(( ${#script_path}+1 ))} == '.git' ]]; then
        symlink "${DIR:(( ${#script_path}+1 ))}"
      fi
    done
    ;;
  '-l')
    for DIR in `find $script_path -mindepth 1 -maxdepth 1 -type d`
    do
      if [[ ! ${DIR:(( ${#script_path}+1 ))} == '.git' ]]; then
        echo "${DIR:(( ${#script_path}+1 ))}"
      fi
    done
    ;;
  *)
    if [[ -n $@ ]]; then
      check_for_stow
      for var in "$@"
      do
        if [[ ! $var == '.git' ]]; then
          symlink "$var"
        fi
      done
    else
      echo "Usage: install.sh [option] [dotfolder]"
      echo "    install.sh                    Shows this help message."
      echo "        -l                        Lists all detected dotfiles."
      echo "        -a                        Installs all detected dotfiles."
      echo "        <dotfolder>               Installs program and dotfiles."
    fi
    ;;
esac
