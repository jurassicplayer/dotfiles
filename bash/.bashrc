#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Custom bash prompt
PS1='[\@ \W]\$> '

# Add undistract-me
source /usr/share/undistract-me/long-running.bash;notify_when_long_running_commands_finish_install

# Environment variables
# Fim font
export FBFONT=/usr/share/kbd/consolefonts/ter-216n.psf.gz

. ~/.config/bash/bashenv
. ~/.config/bash/aliases
