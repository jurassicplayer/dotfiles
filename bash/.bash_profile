#
# ~/.bash_profile
#
[[ -f ~/.bashrc ]] && . ~/.bashrc

# Start X session on login if not SSH
if [[ -z $SSH_CONNECTION ]]; then
    [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
fi
