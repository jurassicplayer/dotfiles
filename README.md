# dotfiles

Cleaner, GNU stow managed dotfiles to facilitate quick and easy migrations to new linux systems.


### Custom Addition                                                                                                                                                                                                            
### Pulseaudio Dynamic range Compression (LADSPA swh-plugins)                                                                                                                                                                  
# set primary audio as default                                                                                                                                                                                                 
# Note: We want primary audio first then switch to compressor audio at                                                                                                                                                         
#       the end to avoid having no sound on boot up. Use the `pacmd list-sinks` to                                                                                                                                             
#       see your audio list.                                                                                                                                                                                                   
#                                                                                                                                                                                                                              
#       e.g: set-default-sink alsa_output.pci-0000_00_14.2.analog-stereo                                                                                                                                                       
###                                                                                                                                                                                                                            
#set-default-sink alsa_output.pci-0000_00_1b.0.analog-stereo  #[your_primary_audio_here]                                                                                                                                       
                                                                                                                                                                                                                        
# Load ladspa module                                                                                                                                                                                                           
.ifexists module-ladspa-sink.so                                                                                                                                                                                                
.nofail                                                                                                                                                                                                                        
load-module module-ladspa-sink sink_name=compressor-stereo plugin=sc4_1882 label=sc4 control=1,1.5,401,-30,20,5,12                                                                                                             
.fail                                                                                                                                                                                                                          
.endif                                                                                                                                                                                                                         
                                                                                                                                                                                                                               
#set-default-sink compressor-stereo
