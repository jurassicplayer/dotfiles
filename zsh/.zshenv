#------------------------------
# Zshenv @ "$HOME"/.zshenv
#------------------------------
export ZVERBOSE=1
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"

# Add ~/.local/bin to path
typeset -U path
path=(~/.local/bin $path[@])

export ZDOTDIR="$HOME"/.config/zsh
#export ZDOTDIR="$HOME"/projects/dotfiles/zsh/.config/zsh