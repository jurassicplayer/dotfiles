#------------------------------
# Zshrc @ "$ZDOTDIR"/.zshrc
#------------------------------
local ZVERBOSE=1
[[ $ZVERBOSE -ge 4 ]] && echo "Loading .zshrc"
[[ $ZVERBOSE -ge 2 ]] && local componentDuration=0
[[ $ZVERBOSE -ge 1 ]] && local bootTimeStart=$(date +%s%N) # For time cop testing

# Zsh Options -----------------
## Changing Directories -------
setopt autocd                       # Type the name of a directory to cd
## Completion -----------------
setopt always_to_end                # When completing from the middle of a word, move the cursor to the end of the word
setopt complete_in_word             # Allow autocomplete from within a word/phrase on both ends (ex. autocompletion "add" returns add_, _add, and _add_)
# setopt menu_complete                # On an ambiguous completion, insert and iterate through matches immediately.
## History --------------------
setopt extended_history             # save timestamp of command and duration
# setopt hist_expire_dups_first       # when trimming history, lose oldest duplicates first
# setopt hist_find_no_dups            # When searching history don't display results already cycled through twice
# setopt hist_ignore_all_dups         # Removes older duplicate commands from history (even if it's not the previous event)
setopt hist_ignore_dups             # Prevents adding current line to history if current == previous
setopt hist_ignore_space            # Remove command lines from the history list when the first character on the line is a space
setopt hist_no_store                # Remove the history (fc -l) command from the history list when invoked. The command lingers in the internal history until the next command is entered.
# setopt hist_reduce_blanks           # Remove extra blanks from each command line being added to history
setopt hist_verify                  # don't execute, just expand history
setopt inc_append_history           # Append history list to the history file (important for multiple parallel zsh sessions!)
setopt share_history                # Import new commands from the history file also in other zsh-session
## Input/Output ---------------
setopt correctall                   # spelling correction for arguments
setopt no_flow_control              # Disables ctrl-s/ctrl-q output freezing
setopt interactive_comments         # Turns on interactive shell comments; comments begin with a #.
## Prompting ------------------
setopt prompt_subst                 # Enable parameter expansion, command substitution, and arithmetic expansion in the prompt
# setopt transient_rprompt            # only show the rprompt on the current prompt
## Zle ------------------------
setopt no_beep                      # don't beep on error
# History configuration -------
[[ -z "$XDG_DATA_HOME" ]] && export XDG_DATA_HOME="$HOME/.local/share"
[[ ! -d "$XDG_DATA_HOME"/zsh ]] && mkdir "$XDG_DATA_HOME"/zsh
export HISTFILE="$XDG_DATA_HOME"/zsh/zsh_history
export HISTSIZE=10000
export SAVEHIST=1000
#------------------------------


# Dynamic Component Loader ----
local components=()
components+=("$ZDOTDIR"/components/*.zsh)
components+=("$ZDOTDIR"/.p10k.zsh)
components+=("$HOME"/.local/local.zsh) # Source local settings (things that shouldn't be synced online)
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)

# Source all files in array
[[ $ZVERBOSE -ge 2 ]] && local componentTimeStart=$(date +%s%N) # For time cop testing
for file in "${components[@]}"; do
  [[ $ZVERBOSE -ge 3 ]] && local sourceIncludeTimeStart=$(date +%s%N) # For time cop testing
  [[ -a "$file" ]] && source "$file"
  if [[ $ZVERBOSE -ge 3 ]]; then # For time cop testing
    # Faster to use an expanded if statement than a one-line if statement chained
    sourceIncludeDuration=$((($(date +%s%N) - $sourceIncludeTimeStart)/1000000))
    echo $sourceIncludeDuration ms runtime for `basename "$file"`
  fi
done
if [[ $ZVERBOSE -ge 2 ]]; then # For time cop testing
  componentDuration=$((($(date +%s%N) - $componentTimeStart)/1000000))
  echo $componentDuration ms overall component duration
fi

# zsh parameter completion for the dotnet CLI
_dotnet_zsh_complete()
{
  local completions=("$(dotnet complete "$words")")
  reply=( "${(ps:\n:)completions}" )
}
compctl -K _dotnet_zsh_complete dotnet

if [[ $ZVERBOSE -ge 1 ]]; then # For time cop testing
  bootTimeDuration=$((($(date +%s%N) - $bootTimeStart)/1000000))
  echo $bootTimeDuration ms overall boot duration
fi

### OTHER OPTIONS ###
# # launches tmux in each new terminal if tmux is not already running in that terminal
# # requires 'tmux' be installed
# ENABLE_TMUX="TRUE"
# # launch new zsh sessions in dtach which allows programs to keep running if terminal is closed/crashes
# # requires 'dtach' be installed
# ENABLE_DTACH="FALSE"

# ### START TMUX/DTACH IF ENABLED IN PROMPT CONFIG ###
# if [[ "$ENABLE_TMUX" = "TRUE" ]] || [[ "$ENABLE_DTACH" = "TRUE" ]]; then
#   # do not run in TTY
#   if [[ ! "$TTY" =~ "/dev/tty" ]]; then
#     # do not start in these programs
#     case $(ps -p $(ps -p $$ -o ppid=) o args=) in
#       *tmux*|*code*|*xterm*|*kdevelop*|*ascii*|*vscodium*|*screen*|*SCREEN*|*dtach*)
#         sleep 0
#         ;;
#       *)
#         if [[ "$ENABLE_TMUX" = "TRUE" && "$ENABLE_DTACH" != "TRUE" ]]; then
#           session="$(tmux ls | grep -v attached | awk 'match($0, /^[0-9]+:/){rc = 1; print substr($0, RSTART, RLENGTH-1)}; END { exit !rc }' | sort -g | head -n1)"
#           if [[ $session ]]; then
#             tmux send-keys -t "$session" " cd \"$PWD\" && clear" Enter
#             tmux attach -t "$session"
#           else
#             tmux
#           fi
#         fi
#         [[ "$ENABLE_DTACH" = "TRUE" && "$ENABLE_TMUX" != "TRUE" ]] && dtach -A /tmp/dtach1234 -z zsh
#         ;;
#     esac
#   fi
# fi