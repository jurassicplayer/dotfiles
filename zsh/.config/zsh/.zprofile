#------------------------------
# Zprofile @ "$ZDOTDIR"/.zprofile
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename .zprofile`"

# Stop Retroarch from picking up vulkan-intel instead of nvidia vulkan icd
# vulkan-intel required for everything that isn't using nvidia card? youtube videos became laggy
export VK_ICD_FILENAMES='/usr/share/vulkan/icd.d/nvidia_icd.json'

# XDG Base Directories --------
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_CACHE_HOME="$HOME"/.cache
export XDG_DATA_HOME="$HOME"/.local/share
export XDG_BIN_HOME="$HOME"/.local/bin

# XDG User Directories --------
export XDG_DESKTOP_DIR="$HOME"/desktop
export XDG_DOCUMENTS_DIR="$HOME"/documents
export XDG_DOWNLOAD_DIR="$HOME"/dwnloads
export XDG_MUSIC_DIR="$HOME"/media/music
export XDG_PICTURES_DIR="$HOME"/media/images
export XDG_PUBLICSHARE_DIR="$HOME"/documents/public
export XDG_TEMPLATES_DIR="$HOME"/documents/templates
export XDG_VIDEOS_DIR="$HOME"/media/videos

# XDG ~/ Workarounds ----------
# https://wiki.archlinux.org/index.php/XDG_Base_Directory#Supported

# https://wiki.archlinux.org/index.php/XDG_Base_Directory#Partial
# export ATOM_HOME="$XDG_DATA_HOME"/atom
export CARGO_HOME="$XDG_CACHE_HOME"/cargo
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/.nv
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
# export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc #Useless for Plasma?
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
export KDEHOME="$XDG_CONFIG_HOME"/kde4
export LESSHISTFILE=-
# export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export GOPATH="$XDG_DATA_HOME"/go
export PLATFORMIO_CORE_DIR="$XDG_DATA_HOME"/platformio
export PYLINTHOME="$XDG_CACHE_HOME"/pylint
export PYTHON_EGG_CACHE="$XDG_CACHE_HOME/python-eggs"
export PYTHONSTARTUP="$XDG_BIN_HOME"/pystartup.py
export RANDFILE="$XDG_DATA_HOME/openssl/rnd"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
# export TASKRC="$XDG_CONFIG_HOME"/task/taskrc
# export WEECHAT_HOME="$XDG_CONFIG_HOME"/weechat
# export WGETRC="$XDG_CONFIG_HOME/wgetrc" && alias wget='wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
# export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
# export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
# xrdb -load "$XDG_CONFIG_HOME"/X11/Xresources

# Inconvenient workarounds ----
# - Note: Aliases don't get set in .zprofile
# alias mocp='mocp -M "$XDG_CONFIG_HOME"/moc'
# alias nvidia-settings='nvidia-settings --config="$XDG_CONFIG_HOME"/nvidia/settings'
# alias tmux='tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf'
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
# VSCode Portable keeps the contents of .vscode folder contained in the directory
# If splitting the folder is preferred, need to symlink relevant folders/files
export VSCODE_PORTABLE="$XDG_CACHE_HOME"/Code
# NPM requires a custom configuration containing:
# npmrc -----------------------
# prefix=${XDG_DATA_HOME}/npm
# cache=${XDG_CACHE_HOME}/npm
# tmp=${XDG_RUNTIME_DIR}/npm
# init-module=${XDG_CONFIG_HOME}/npm/config/npm-init.js
#------------------------------
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
# export NVM_DIR="$XDG_DATA_HOME"/nvm

# https://wiki.archlinux.org/index.php/XDG_Base_Directory#Hardcoded
# export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android

# VIM requires a custom configuration containing:
# vimrc -----------------------
# set undodir=$XDG_DATA_HOME/vim/undo
# set directory=$XDG_DATA_HOME/vim/swap
# set backupdir=$XDG_DATA_HOME/vim/backup
# set viminfo+=n$XDG_DATA_HOME/vim/viminfo
# set runtimepath=$XDG_CONFIG_HOME/vim,$VIMRUNTIME,$XDG_CONFIG_HOME/vim/after
#------------------------------
# mkdir -p "$XDG_DATA_HOME"/vim/{undo,swap,backup}
# export VIMINIT=":source $XDG_CONFIG_HOME"/vim/vimrc

# Start X if not started yet
if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx "$XDG_CONFIG_HOME"/X11/xinitrc
fi
