# Rename to bootstrap.zsh?
# Have it manage all of my configs instead of a separate thing?
# Make folders, Install programs/plugins, Symlink everything
# Remove as well
# All variables should be stored in .local/local.zsh?

# Process if no one-time-install flag in .local.zsh

  # Install dependencies for terminal lyfe if not installed
  # Make a .local/share/zsh folder (for history file)
  # Make a .local/share/gnupg folder
  # Make a .local/share/wineprefixes
  # Make a .config/git/config file
  #[[ ! -d "$XDG_DATA_HOME"/wineprefixes ]] && mkdir -p "$XDG_DATA_HOME"/wineprefixes
  # zinit
  # git
  # yay
  # tmux
  # nnn
  # atool
  # nvim
  # jq
  # powerline font: nerd-fonts-droid-sans-mono

  # create symlinks

  # Set one-time-install flag to .local/local.zsh
