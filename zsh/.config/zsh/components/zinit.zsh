#------------------------------
# Zdharma/ZInit
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"

# Set up zinit paths
typeset -A ZINIT
ZINIT[HOME_DIR]="$XDG_DATA_HOME"/zinit
ZINIT[ZCOMPDUMP_PATH]=$XDG_CACHE_HOME/zsh/zcompdump

# Automatic installation if zinit isn't found
if [[ ! -f ${ZINIT[HOME_DIR]}/bin/zinit.zsh ]]; then
	git clone https://github.com/zdharma-continuum/zinit ${ZINIT[HOME_DIR]}/bin
fi

source "${ZINIT[HOME_DIR]}"/bin/zinit.zsh

# Add plugins to zinit
zinit ice lucid depth"1"
zinit light romkatv/powerlevel10k

zinit ice wait lucid atclone"dircolors -b LS_COLORS > clrs.zsh" atpull'%atclone' pick"clrs.zsh" nocompile'!'
zinit light "trapd00r/LS_COLORS"

zinit light-mode lucid for \
  zsh-users/zsh-autosuggestions \
  zdharma/fast-syntax-highlighting \
  MichaelAquilina/zsh-auto-notify
zinit light-mode wait lucid atload"zicompinit; zicdreplay" blockf for \
  zsh-users/zsh-completions

zstyle ":completion:*:cd:*" ignore-parents parent pwd
zstyle ":completion:*" auto-description "specify: %d"
zstyle ":completion:*" format "Completing %d"
zstyle ":completion:*" group-name ""
zstyle ":completion:*" menu select
zstyle ":completion:*:descriptions" format "%B%d%b"
zstyle ":completion:*" list-colors “${(s.:.)LS_COLORS}”
zstyle ":completion:*" list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ":completion:*" matcher-list "" "m:{a-z}={A-Z}" "m:{a-zA-Z}={A-Za-z}" "r:|[._-]=* r:|=* l:|=*"
zstyle ":completion:*" select-prompt %SScrolling active: current selection at %p%s
zstyle ":completion:*" use-compctl true
zstyle ":completion:*" verbose true
zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#)*=0=01;31"
zstyle ":completion:*:kill:*" command "ps -u $USER -o pid,%cpu,tty,cputime,cmd"
