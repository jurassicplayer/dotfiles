#------------------------------
# Suzumiya Functions
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"
#local NAGATO_TAG="$(tput setab 5) $(tput setaf 7)YUKI.N $(tput sgr0)$(tput setaf 5)$(tput sgr0) "
local NAGATO_TAG=" YUKI.N❯  "
local TAG_SIZE=10
nagato () {
  [[ $NAGATO ]] && return;
  clear
  slow_read "${NAGATO_TAG}If you are reading this,"
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "I'm probably no longer myself."
  echo "" && sleep 1
  slow_read "${NAGATO_TAG}When this message appears,"
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "it means that you, me, Suzumiya Haruhi, Asahina Mikuru, and"
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "Koizumi Itsuki are all present."
  echo "" && sleep 1
  slow_read "${NAGATO_TAG}That's the key."
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "You have found the answer."
  echo "" && sleep 1
  slow_read "${NAGATO_TAG}This is an emergency escape program."
  echo "" && sleep 1
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "To activate it, press Enter,"
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "otherwise, press any other key."
  echo "" && sleep 1
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "If you activate it,"
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "you will be given a chance to repair the space-time continuum."
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "However, there is no guarantee of success."
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "There is also no guarantee of your safe return."
  echo "" && sleep 1
  slow_read "${NAGATO_TAG}This program can be started only once."
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "Once executed, it will be erased."
  echo "" && sleep 1
  printf ' %.0s' {1..${TAG_SIZE}} && slow_read "If you choose not to activate it, it will also be erased."
  echo "" && sleep 1
  sleep 2 && printf ' %.0s' {1..${TAG_SIZE}} && slow_read "Ready?"
  NAGATO=1
  read reply
  [[ $reply != "" ]] && return;
  echo "You pressed enter."
}

function slow_read() {
  local DELAY=0.05
  while getopts 'd:' flag; do
    case "${flag}" in
      d) DELAY="${OPTARG}" ;;
      *) error "Unexpected option ${flag}" ;;
    esac
  done
  shift $((OPTIND -1))
  s=$@
  while [ ${#s} -gt 0 ]; do
    printf '%s' "${s%${s#?}}"
    s=${s#?}
    sleep $DELAY
  done
  printf '\n'
  sleep $DELAY
}