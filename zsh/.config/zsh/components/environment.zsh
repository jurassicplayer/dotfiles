#------------------------------
# Environment
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"
[[ -f /usr/bin/nvim ]] && export EDITOR=nvim || export EDITOR=nano
export BROWSER=xdg-open

# Set standard terminal variable for SSH sessions
export TERM=xterm-256color

# Make Vi mode transitions faster (KEYTIMEOUT is in hundredths of a second)
export KEYTIMEOUT=1

# Colorful less
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

# dotnet tools
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
export PATH="$HOME/.dotnet/tools:$PATH"
