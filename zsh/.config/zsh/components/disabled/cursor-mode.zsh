#------------------------------
# Vim Cursor
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
    [[ $1 = 'block' ]]; then
      echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
    [[ ${KEYMAP} == viins ]] ||
    [[ ${KEYMAP} = '' ]] ||
    [[ $1 = 'beam' ]]; then
      echo -ne '\e[5 q'
  fi
  zle reset-prompt
  zle -R
}

# bindkey -v
# Start every prompt in insert mode
zle-line-init() {
  zle -K viins # Can be removed if `bindkey -v` has been set elsewhere
  # Problem is, without setting prompt to insert mode every time, when
  # in vicmd mode, ctrl+c doesn't reset cursor to I-beam
}

zle -N zle-line-init
zle -N zle-keymap-select

# Reset the cursor style after running applications
preexec() { echo -ne '\e[5 q' ;} 