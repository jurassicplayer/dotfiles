#------------------------------
# zpm-zsh/zpm
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"


if [[ ! -f "$XDG_DATA_HOME"/zpm/zpm.zsh ]]; then
  git clone --recursive https://github.com/zpm-zsh/zpm "$XDG_DATA_HOME"/zpm
fi
source "$XDG_DATA_HOME"/zpm/zpm.zsh

zpm load zdharma/fast-syntax-highlighting,async
zpm load MichaelAquilina/zsh-auto-notify,async
zpm load zsh-users/zsh-autosuggestions,async
zpm load trapd00r/LS_COLORS,hook:"dircolors -b LS_COLORS > clrs.zsh",source:clrs.zsh,async
zpm load zsh-users/zsh-completions

zstyle ":completion:*:cd:*" ignore-parents parent pwd
zstyle ":completion:*" auto-description "specify: %d"
zstyle ":completion:*" format "Completing %d"
zstyle ":completion:*" group-name ""
zstyle ":completion:*" menu select
zstyle ":completion:*:descriptions" format "%B%d%b"
zstyle ":completion:*" list-colors “${(s.:.)LS_COLORS}”
zstyle ":completion:*" list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ":completion:*" matcher-list "" "m:{a-z}={A-Z}" "m:{a-zA-Z}={A-Za-z}" "r:|[._-]=* r:|=* l:|=*"
zstyle ":completion:*" select-prompt %SScrolling active: current selection at %p%s
zstyle ":completion:*" use-compctl true
zstyle ":completion:*" verbose true
zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#)*=0=01;31"
zstyle ":completion:*:kill:*" command "ps -u $USER -o pid,%cpu,tty,cputime,cmd"