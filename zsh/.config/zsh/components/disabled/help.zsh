#------------------------------
# help
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"
# A simpler man-page leveraging cht.sh
function help() {
  curl -s "https://cht.sh/$@"
}