#------------------------------
# Prompt
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"
# Shell Introduction ----------
#echo "$(tput setaf 7)$(tput setab 5) YUKI.N $(tput op)$(tput setaf 5)$(tput setaf 7) Ready?"

# Prompt ----------------------
# PROMPT="%F{black}%K{green} %n %F{green}%K{red}%f" # User
# PROMPT+="%F{black}%K{red} %m %F{red}%K{blue}%f"
# PROMPT+="%F{black}%K{blue} %25<$(echo "/${${PWD#/*}%%/*}/" | sed 's%\/home\/%\~\/%')...<%~ %F{blue}%k%f"
# PROMPT+="
# %f%k❯ "
# export PS1="$PROMPT"
# export PS2="❯❯ "
#------------------------------

prompt_fg_samples () {
  for i in {000..256}; do
    print -P -- "$(tput sgr0)$i: %{$(BG_COLOR $COLOR_BG)%}%${i}F %n %S %25<$(echo "/${${PWD#/*}%%/*}/" | sed 's%\/home\/%\~\/%')...<%~ %s%k%f"
  done
}
prompt_bg_samples () {
  for i in {000..256}; do
    print -P -- "$(tput sgr0)$i: %{$(echo -e "\033[48;5;${i}m")%}%$(FG_COLOR)F %n %S %25<$(echo "/${${PWD#/*}%%/*}/" | sed 's%\/home\/%\~\/%')...<%~ %s%k%f"
  done
}
### FUCTION TO CHANGE COLOR BASED ON $PWD ###
FG_COLOR () {
  case $PWD in
    /usr*|/opt*)
      echo "$COLOR_USR"
      ;;
    $HOME*)
      echo "$COLOR_HOME"
      ;;
    *)
      echo "$COLOR_ROOT"
      ;;
  esac
}
MAIN_COLOR () {
  FG_COLOR
}
###
### FUNCTION TO SET THE BACKGROUND COLOR ###
BG_COLOR () {
  echo -e "\033[48;5;${1}m"
}
BACKGROUND_COLOR () {
  echo -e "\033[48;5;${COLOR_BG}m"
}
### FUNCTIONS TO GET THE GIT STATUS OF THE CURRENT DIRECTORY ###
parse_git_branch () {
  (git symbolic-ref -q HEAD || git name-rev --name-only --no-undefined --always HEAD) 2> /dev/null
}
parse_git_state () {
  if [[ -z "$(git status --porcelain)" ]]; then
    GIT_STATE=""
  else
    GIT_STATE="$(git status --porcelain | wc -l) "
  fi
  if [[ ! -z "$GIT_STATE" ]]; then
    echo "$GIT_STATE"
  fi
}
GIT_STATUS () {
  local git_where="$(parse_git_branch)"
  [ -n "$git_where" ] && echo "%$(FG_COLOR)F%S%{$(BG_COLOR $COLOR_BG)%} ʮ ${git_where#(refs/heads/|tags/)} $(parse_git_state)%s%f%k"
}
###
### FUNCTION TO SET THE EXIT STATUS PROMPT ###
EXIT_STATUS () {
  EXIT="$?"
  case $EXIT in
    0)
      echo ""
      ;;
    *)
      echo "%$(FG_COLOR)F%S%{$(BG_COLOR $COLOR_BG)%} ✘ "$EXIT "%s%f%k"
      ;;
  esac
}
###
# set the contents of the main prompt (PS1)
### FUNCTION TO SET THE PROMPT ###
MAIN_PROMPT () {
  echo -e "$PS1_CONTENTS"
}

### PROMPT OPTIONS ###
# COLORS
# color support is limited to your terminal
# for most terminals, valid colors are 000-256
# run 'prompt_fg_samples' and 'prompt_bg_samples' for a preview of the colors
# set these colors to the same color to disable the prompt changing color based on directory
# foreground color for the prompt when in $HOME directory
COLOR_HOME="004"
# foreground color for the prompt when in /usr/* and /opt/*
COLOR_USR="011"
# foreground color for the prompt when in /*
COLOR_ROOT="009"
# background color for the prompt
COLOR_BG="000"
# PROMPT
# set the contents of the prompt
# '%$(FG_COLOR)F' is the color based on the $PWD as set in ~/.zsh_prompt.conf
# '%{$(BG_COLOR $COLOR_BG)%}' is the background color as set in ~/.zsh_prompt.conf
# '%25<$(echo "/${${PWD#/*}%%/*}/" | sed 's%\/home\/%\~\/%')...<%~' truncates the current
# directory if more than 25 characters
# see http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Prompt-Expansion for zsh prompt expansions
PS1='%{$(BG_COLOR $COLOR_BG)%}%$(FG_COLOR)F %n %S %25<$(echo "/${${PWD#/*}%%/*}/" | sed 's%\/home\/%\~\/%')...<%~ %s%k%f '
# set whether exit status and git status prompt on right side is enabled
# must be set to TRUE or FALSE
ENABLE_RPS1="TRUE"
###

### SET THE PROMPT ###
if [[ -z "$PS1" ]]; then
  PS1='%{$(BG_COLOR $COLOR_BG)%}%$(FG_COLOR)F %n %S %25<$(echo "/${${PWD#/*}%%/*}/" | sed 's%\/home\/%\~\/%')...<%~ %s%k%f '
fi
if [[ "$ENABLE_RPS1" = "TRUE" ]]; then
  RPS1='$(EXIT_STATUS)$(GIT_STATUS)'
else
  unset RPS1
fi
###