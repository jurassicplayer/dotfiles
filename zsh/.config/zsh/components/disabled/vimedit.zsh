#------------------------------
# Vim Edit
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"

# Enable ctrl+e to edit command line
autoload -U edit-command-line
# Vi style:
zle -N edit-command-line
bindkey '^e' edit-command-line