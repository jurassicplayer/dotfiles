#------------------------------
# Color Table
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"
function fgtab(){
    echo "tput setaf/setab - Foreground/Background table"
    for f in {0..15}; do
        for b in {0..15}; do
            printf -v i "%02d/%02d" $f $b
            echo -en "$(tput setaf $f)$(tput setab $b) ${i}"
        done
        echo -e "$(tput sgr 0)"
    done
}
