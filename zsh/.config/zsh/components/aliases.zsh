#------------------------------
# Aliases
#------------------------------
[[ $ZVERBOSE -ge 4 ]] && echo "Loading `basename "$0"`"
# Inconvenient XDG workarounds -
# alias mocp='mocp -M "$XDG_CONFIG_HOME"/moc'
alias nvidia-settings='nvidia-settings --config="$XDG_CONFIG_HOME"/nvidia/settings'
alias steam='HOME="$XDG_DATA_HOME"/Steam steam'
alias tmux='tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf'

# Global shortcuts
alias -g L="| less"
alias -g H="| head"
alias -g T="| tail"
alias -g G="| grep --color=auto"
alias -g N=">/dev/null"

# Suffix shortcuts
alias -s html='$BROWSER'
alias -s com='$BROWSER'
alias -s org='$BROWSER'
alias -s .git='git clone'

# Miscellaneous
alias cursor-hide="printf '\e[?25l'"
alias cursor-show="printf '\e[?25h'"

# Common Programs
[[ -f /usr/bin/nvim ]] && alias vim='nvim'

# Pacman aliases
[[ -f /usr/bin/paru ]] && alias pacman='paru'
#alias paccache='pacman -Sc'
alias pacorphan='pacman -Rns $(pacman -Qtdq)'
alias pacpkg="pacman -Qqe >> packagelist.txt ; pacman -Qqe | fzf --preview='pacman -Qi {}' --preview-window=':80%' --height='100%' --bind='enter:execute(pacman -Qi {} | less)'"
alias pacreflect='sudo reflector --verbose --country "United States" -l 20 -p http --sort rate --save /etc/pacman.d/mirrorlist'
alias pacaur="pacman -Qmq | fzf --preview='pacman -Qi {}' --preview-window=':80%' --height='100%' --bind='enter:execute(pacman -Qi {} | less)'"

# CLI Eyecandy aliases
[[ -f /usr/bin/exa ]] && alias ls='exa --color=auto -lha' || alias ls='ls --color=auto -la'
alias diff='diff --color=auto'
alias dmesg='dmesg --color=auto'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias lsmnt="mount | awk -F' ' '{ printf \"%s\t%s\n\",\$1,\$3; }' | column -t | egrep ^/dev/ | sort"
[[ -f /usr/bin/rsync ]] && alias cpv='rsync -ah --info=progress2'
alias neofetch='neofetch --block_range 0 15'

# Zsh aliases
alias zenv='$EDITOR $ZDOTDIR/components/environment.zsh'
alias zalias='$EDITOR $ZDOTDIR/components/aliases.zsh'
alias zplugins='$EDITOR $ZDOTDIR/components/zinit.zsh'
alias zprofile='$EDITOR $ZDOTDIR/.zprofile'
alias zshrc='$EDITOR $ZDOTDIR/.zshrc'
alias zsrc='source $ZDOTDIR/.zshrc'

# Git aliases
alias gac="git add -A && git commit -m "
alias gstat="git status"
alias gpull="git pull origin"
alias gpush="git push origin"

# Human-readable
alias du='du -h'
alias free='free -m'

# Lazy shit
alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'
alias sddm-greeter='sddm-greeter --test-mode --theme $PWD'
alias untar='tar -zxvf '
alias www='python -m http.server'
alias calibre-sync='rsync -asP --delete "'$HOME'/Calibre Library"/* "jplayer@192.168.1.111:/data/.appdata/calibre/Calibre Library"'

# Clear
alias c='clear'
alias claer='clear'
alias clae='clear'
alias clera='clear'
alias ckear='clear'
alias clr='clear'
alias clear='[ $[$RANDOM % 30] = 0 ] && slow_read -d 0.01 "I-it'"'"'s not like I wanted to clear your terminal or anything." && sleep 0.5; clear || clear'

# Alias functions
# Use awk as a calculator
calc () {
    awk "BEGIN {print $@}"
}

fgtab () {
    echo "tput setaf/setab - Foreground/Background table"
    for f in {0..15}; do
        for b in {0..15}; do
            printf -v i "%02d/%02d" $f $b
            echo -en "$(tput setaf $f)$(tput setab $b)${i}"
        done
        echo -e "$(tput sgr 0)"
    done
}

# Use nnn to switch directories
n () {
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "export" as in:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn -x "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

# Net reliant alias functions
# Use owlbot.info to get definitions; requires jq
define () { curl -sL "https://owlbot.info/api/v2/dictionary/"$1"?format=json" | jq -r '.' }

#Check to see if site is down for you or everyone
downforme () {
    wget -qO - "http://downforeveryoneorjustme.com/$1" | grep -qo "It's just you" && echo -e "$(tput setaf 1)It's just you.\n$(tput setaf 2)$1 is up.$(tput sgr0)" || echo -e "$(tput setaf 3)It's not just you! \n$(tput setaf 1)$1 looks down from here.$(tput sgr0)"
}

# Use cht.sh to get simple help outputs for all sorts of Linux things
help () { curl -s "https://cht.sh/$@" }

# Search for and play a youtube video
mpvyt () { mpv ytdl://ytsearch10:"$*" }

dockerhub () {
    TOKEN=$(curl "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull" | jq -r .token)
    curl --head -H "Authorization: Bearer $TOKEN" https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest 2>&1 | grep RateLimit
}
