#!/bin/bash



ModificaScript="0"
ModificaDesktopManuale="0"
ModificaDesktopGuidato="0"
Elimina="0"

Scelta=$(kdialog --geometry 500x200+3000 --menu " " 0 "Edit Script" 1 "Manual Edit .desktop  ( mime-type,Name,Submenu,etc )" 3 "Step-by-step Edit .desktop  ( mime-type,Name,Submenu,etc )" 2 "Remove Action")
[ "$?" = "1" ] && exit 1

#kdialog --passivepopup "$Scelta"

case $Scelta in
"0") ModificaScript="1" 
;;
"1") ModificaDesktopManuale="1"
;;
"2") Elimina="1"
;;
"3") ModificaDesktopGuidato="1"
esac


Lista=$(ls -1 $HOME/.bin/MenuPersonalizzato | sed 's/ /\^/g' )

[ "$Lista" != "" ]  ||  kdialog --title "Error"  --passivepopup "No Action Found"



if [ $ModificaScript = "1" ]
then

Nome=$(kdialog --geometry 350x100+30000 --combobox "Edit Action" $Lista | sed 's/\^/ /g')
if [ $? = "0" ] && [ "$Nome" != "" ] && [ -f "$HOME/.bin/MenuPersonalizzato/$Nome" ]
then
if [ -e /usr/bin/kate ]
then
/usr/bin/kate "$HOME/.bin/MenuPersonalizzato/$Nome"
elif [ -e /usr/bin/kwrite ]
then
/usr/bin/kwrite "$HOME/.bin/MenuPersonalizzato/$Nome"
else
kdialog --title "No Text Editor Found" --passivepopup "Please Install Kate or Kwirte"
fi
fi


elif [ "$Elimina" = "1" ]
then
Nome=$(kdialog --geometry 350x100+30000 --combobox "Remove Action" $Lista | sed 's/\^/ /g')
if [ $? = "0" ] && [ "$Nome" != "" ] && [ -f "$HOME/.bin/MenuPersonalizzato/$Nome" ]
then
rm "$HOME/.bin/MenuPersonalizzato/$Nome"
rm "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"
fi
fi

if [ "$ModificaDesktopManuale" = "1" ]
then
Nome=$(kdialog --geometry 350x100+30000 --combobox "Edit Action.desktop" $Lista | sed 's/\^/ /g')
if [ $? = "0" ] && [ "$Nome" != "" ] && [ -f "$HOME/.bin/MenuPersonalizzato/$Nome" ]
then
if [ -e /usr/bin/kate ]
then
/usr/bin/kate "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"
elif [ -e /usr/bin/kwrite ]
then
/usr/bin/kwrite "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"
else
kdialog --title "No Text Editor Found" --passivepopup "Please Install Kate or Kwirte"
fi
fi
fi






if [ "$ModificaDesktopGuidato" = "1" ]
then
Nome=$(kdialog --geometry 350x100+30000 --combobox "Edit Action.desktop" $Lista | sed 's/\^/ /g')
[ "$Nome" = "" ] && exit 1









while true;
do
Scelta2=$(kdialog --geometry 350x500+3000 --menu "Select An Option" 1 "Change Name" 2 "Change Icon" 6 "Change Mime-Type" 3 "Change Sub-Menu Name" 4 "Enable/Disable Sub-Menu" 5 "Show Inside/Outside Dolphin Actions Menu" )
[ "$?" = "1" ] && exit 1



case $Scelta2 in
"1") CambiaNome="1" 
;;
"2") CambiaIcona="1"
;;
"3") CambiaNomeMenu="1"
;;
"4") DisabilitaMenu="1"
;;
"5") MostraNelMenuAzioni="1"
;;
"6") CambiaMimeType="1"
esac





if [ "$CambiaNome" = "1" ]
then
NuovoNome=$(kdialog  --inputbox "Set New Name                                    ")
if [ "$?" != "1" ] && [ "$NuovoNome" != "" ] 
then
sed -i "s/^Name=.*/Name=$NuovoNome/"  "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"
kdialog --passivepopup "Name Changed"
fi

elif [ "$CambiaIcona" = "1"  ]
then 

NuovaIcona=$(kdialog  --geticon)
if [ "$?" != "1" ]
then
sed -i "s/^Icon=.*/Icon=$NuovaIcona/"  "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"
kdialog --passivepopup "Icon Changed"
fi

fi

if [ "$CambiaNomeMenu" = "1" ]
then
NuovoNomeMenu=$(kdialog  --inputbox "Set New Sub-Menu Name                              " | sed 's/ *$//' )
if [ "$?" != "1" ] && [ "$NuovoNomeMenu" != "" ] 
then
sed -i "s/^X-KDE-Submenu.*/X-KDE-Submenu=$NuovoNomeMenu/"  "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"
sed -i "s/^#X-KDE-Submenu.*/#X-KDE-Submenu=$NuovoNomeMenu/"  "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"
kdialog --passivepopup "Sub-Menu Name Changed"

fi



elif [ "$DisabilitaMenu" = "1" ]
then

Disabilitasino=$(kdialog --geometry 400x100+3000 --menu  "" 1 "Disable Sub-Menu" 2 "Enable Sub-Menu")

if [ "$?" != "1" ]
then


if [ "$Disabilitasino" = "1"   ]
then 
sed -i 's/^X-KDE-Submenu/#X-KDE-Submenu/' "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"

elif [ "$Disabilitasino" = "2"   ] 
then
sed -i 's/^#X-KDE-Submenu/X-KDE-Submenu/' "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"

fi



fi
fi

if [ "$MostraNelMenuAzioni" = "1" ]
then

Mostra=$(kdialog --geometry 400x100+3000 --menu  "" 1 "Show Outside Dolphin Actions Menu" 2 "Show Inside Dolphin Actions Menu")

if [ "$Mostra" = "1"   ]
then 
sed -i 's/^#X-KDE-Priority=TopLevel/X-KDE-Priority=TopLevel/' "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"

elif [ "$Mostra" = "2"   ] 
then
sed -i 's/^X-KDE-Priority=TopLevel/#X-KDE-Priority=TopLevel/' "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"

fi


elif [ "$CambiaMimeType" = "1" ]
then

TipoAttuale=$(grep ^MimeType= "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop" | cut -d '=' -f2 )

Tipo=$(kdialog --inputbox "Set MimeType                                         " "$TipoAttuale" | tr -d ' ')

[ "$Tipo" != ""  ] && sed -i "s|^MimeType=.*|MimeType=$Tipo|" "$HOME/.local/share/kservices5/ServiceMenus/$Nome.desktop"


fi











CambiaNome="0"
CambiaIcona="0"
CambiaNomeMenu="0"
DisabilitaMenu="0"
MostraNelMenuAzioni="0"
NuovoNome=" "
NuovaIcona=" "
NuovoNomeMenu=" "
Disabilitasino=""
Mostra=" " 
CambiaMimeType="0"
Tipo=" "
TipoAttuale=" "







done






fi












