 #!/bin/bash
[[ ! -f /usr/bin/atool ]] && return
TARGET_FMT=''
FILES=''
while getopts 'f:' flag; do
  case "${flag}" in
    f) TARGET_FMT="${OPTARG}" ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done
shift $((OPTIND -1))
local CB_FMT=( "cb7" "cbr" "cbz" "cbt" "cba" )
local BASE_FMT
local INTERMEDIATE_FMT
local DEL_INTERMEDIATE
[[ "$TARGET_FMT" == "cb7" ]] && INTERMEDIATE_FMT="7z"
[[ "$TARGET_FMT" == "cbr" ]] && INTERMEDIATE_FMT="rar"
[[ "$TARGET_FMT" == "cbz" ]] && INTERMEDIATE_FMT="zip"
[[ "$TARGET_FMT" == "cbt" ]] && INTERMEDIATE_FMT="tar"
[[ "$TARGET_FMT" == "cba" ]] && INTERMEDIATE_FMT="ace"
for f in "$@"
do
    fname=$(basename -- "$f")
    ext="${fname#*.}"
    fname=$(basename "$fname" ."$ext")

    [[ "$ext" == "$TARGET_FMT" ]] && notify-send "Skipped" "$fname"."$ext" && continue  # If the target format is the same as the current, skip file
    [[ "$ext" == "cb7" ]] && BASE_FMT="7z"
    [[ "$ext" == "cbr" ]] && BASE_FMT="rar"
    [[ "$ext" == "cbz" ]] && BASE_FMT="zip"
    [[ "$ext" == "cbt" ]] && BASE_FMT="tar"
    [[ "$ext" == "cba" ]] && BASE_FMT="ace"

    [[ -d "$f" ]] && tar -czvf "$fname".tar.gz "$fname" && ext="tar.gz" && DEL_INTERMEDIATE=1       # If the file is a directory, create .tar.gz, change ext, and flag to delete
    [[ ${CB_FMT[@]#"$ext"} != ${CB_FMT[@]} ]] && [[ -f "$fname"."$ext" ]] && mv "$fname"."$ext" "$fname"."$BASE_FMT" && ext="$BASE_FMT" && DEL_INTERMEDIATE=1 # If the file is already a comic book format, convert to base format, change ext, and flag to delete
    [[ -f "$fname"."$ext" ]] && arepack "$fname"."$ext" "$fname"."$INTERMEDIATE_FMT"                # Convert archive formats using arepack
    [[ $? -eq 1 ]] && notify-send "Arepack conversion failed" "$fname"."$ext" && continue           # If arepack fails for some reason, skip to next file
    [[ $DEL_INTERMEDIATE ]] && rm "$fname"."$ext"                                                   # Delete flagged files, this only occurs if the arepack conversion did not fail
    [[ -f "$fname"."$INTERMEDIATE_FMT" ]] && mv "$fname"."$INTERMEDIATE_FMT" "$fname"."$TARGET_FMT" # Change file extension to comic book equivalent
    notify-send "Converted to $TARGET_FMT" "$fname"."$TARGET_FMT"
done