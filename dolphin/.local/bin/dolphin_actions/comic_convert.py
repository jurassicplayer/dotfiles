#!/usr/bin/env python3
import os, argparse, subprocess, zipfile, shutil

# RAR support requires the proprietary rar binary
a_formats = ['7z',  'rar', 'zip', 'tar']
c_formats = ['cb7', 'cbr', 'cbz', 'cbt']

class FormatError(Exception):
    pass
class SubprocessError(Exception):
    pass

def parser():
  parser = argparse.ArgumentParser(description="Pack or convert a series of folders/archives into a specified comic book format.")
  parser.add_argument('--format', '-f', dest='fmt', action='store', type=str, default='cb7', help='Specify target comic book format (cb7/cbr/cbz/cbt)')
  parser.add_argument('--output_dir', '-o', dest='output_dir', action='store', type=str, help='(Optional) Specify an output directory')
  parser.add_argument('--delete', '-d', dest='delete_origin', action='store_true', help='(Optional) Delete the original files')
  parser.add_argument('file_list', metavar='file', type=str, nargs='+', help='Folder/file list to be converted')
  return parser

def convert_dir(folder_path, fmt, delete_origin=False, output_dir=None):
  #notify_send("debug", 'dir-to-{} (-d: {} | -o: {}): {}'.format(fmt, delete_origin, output_dir, folder_path))
  dirname = os.path.dirname(folder_path)
  basename = os.path.basename(folder_path)
  if fmt not in c_formats: raise FormatError
  if not output_dir: output_dir = dirname

  file_path = os.path.join(output_dir, '{}.zip'.format(basename))

  with zipfile.ZipFile(file_path, 'w') as archive:
    for filename in os.listdir(folder_path):
      archive.write(os.path.join(folder_path,filename), arcname=filename)
  convert_file(file_path, fmt, delete_origin=True)
  if delete_origin: shutil.rmtree(folder_path)

def convert_file(file_path, fmt, delete_origin=False, output_dir=None):
  #notify_send("debug", 'dir-to-{} (-d: {} | -o: {}): {}'.format(fmt, delete_origin, output_dir, file_path))
  dirname = os.path.dirname(file_path)
  basename, baseext = os.path.splitext(os.path.basename(file_path))

  if (baseext[1:] not in a_formats) and (baseext[1:] not in c_formats): raise FormatError
  if fmt not in c_formats: raise FormatError
  if not output_dir: output_dir = dirname
  
  base_file_path = file_path
  if baseext[1:]in c_formats:
    # Change comic book formats into archive formats
    base_file_path = comicize(base_file_path)
    basename, baseext = os.path.splitext(os.path.basename(base_file_path))
  intermediate_file_ext = a_formats[c_formats.index(fmt)]
  intermediate_file_path = os.path.join(output_dir, "{}.{}".format(basename, intermediate_file_ext))

  if baseext[1:] != intermediate_file_ext:
    # If the archive format needs to be changed, repack and delete the original
    result = subprocess.run(['arepack', base_file_path, intermediate_file_path])
    if result.returncode: raise SubprocessError
    if delete_origin: os.remove(base_file_path)
  comicize(intermediate_file_path)

def comicize(file_path):
  # Swap file extensions between equivalents and return new file path. Ex: zip <-> cbz, 7z <-> cb7, etc.
  filename, oldext = os.path.splitext(file_path)
  if oldext[1:] in a_formats:
    old_formats = a_formats
    new_formats = c_formats
  elif oldext[1:] in c_formats:
    old_formats = c_formats
    new_formats = a_formats
  new_fmt = new_formats[old_formats.index(oldext[1:])]
  new_file_path = '{}.{}'.format(filename, new_fmt)
  os.rename(file_path, new_file_path)
  return new_file_path

def notify_send(title, message):
  subprocess.run(['notify-send', title, message])

if __name__ == "__main__":
  parser = parser()
  args = parser.parse_args()
  if args.fmt == 'cbr' and not shutil.which('rar'): 
    notify_send('Unsupported format', 'RAR support requires proprietary rar binary in path.')
  else:
    for path in args.file_list:
      path = os.path.abspath(path)
      basename = os.path.basename(path)
      if not os.path.exists(path): continue
      try:
        if os.path.isdir(path): convert_dir(path, args.fmt, delete_origin=args.delete_origin, output_dir=args.output_dir)
        elif os.path.isfile(path): convert_file(path, args.fmt, delete_origin=args.delete_origin, output_dir=args.output_dir)
        else: raise FormatError
        notify_send('Converted:', basename)
      except FormatError:
        notify_send('Skipped {}'.format(basename), 'Unsupported format type')
        #print("Unsupported format type: {}".format(path))
      except SubprocessError:
        notify_send('Skipped {}'.format(basename), 'Failed atool subprocess')
        #print("Failed atool subprocess: {}".format(path))