#!/bin/bash

Nome=$(kdialog --inputbox "Set Name                                              " |  sed 's/ *$//' | sed 's/^ *//')
[ "$Nome" != "" ] || exit 1
Icona=$(kdialog --geticon)
[ "$Icona" != "" ]  || exit 1

kdialog --geometry 450x600 --textinputbox "script" "#!/bin/bash \n# \$1 is the current location   " > ~/.bin/MenuPersonalizzato/"$Nome"


echo "
[Desktop Entry]
Type=Service
ServiceTypes=KonqPopupMenu/Plugin
#the mime-type determines on what type of file the action should appear
#all/all will make it appear anywhere
#all/allfiles will make it appear only on files and not on folders
#inode/directory will make it appear only on the folders
#you can use file -b --mime-type /path/to/file to discover the mime-type of a file
#you can append several mime-types to make it appear on more than one
# for example: MimeType=image/jpeg;image/png; will make it appear on all png and jpg files 
MimeType=all/all;
Actions= $Nome;
X-KDE-StartupNotify=false
Icon=gnome-networktool
#if X-KDE-Priority=TopLevel is commented, the submenu will be shown in dolphin actions menu
X-KDE-Priority=TopLevel
#with X-KDE-Submenu you can define the name of the sub-menu
#if this option is removed the custom action will be shown directly without any sub-menu.
X-KDE-Submenu=Custom Menu
X-KDE-Submenu[it]=Menu Personalizzato


[Desktop Action $Nome]
#here you can change the name of your custom action
Name=$Nome
#here you can change the icon: to find the name of the icons you can write this ( kdialog --geticon  ) in the terminal and select an icon 
#the name of the icon will be shown in the output of the terminal
Icon=$Icona
Exec=/bin/bash \"$HOME/.bin/MenuPersonalizzato/$Nome\" " > ~/.local/share/kservices5/ServiceMenus/"$Nome".desktop
